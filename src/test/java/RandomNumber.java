import java.util.Scanner;
public class RandomNumber {
    public static void main(String[] args) {
        System.out.println("Please enter your Name:)");
        Scanner console = new Scanner(System.in);
        String name = console.nextLine();

            System.out.println("Let the game begin!");
            int MyNumber = 38;
            int number;
        do {
            System.out.println("Guess my number between 0 and 100: ");
            number = console.nextInt();
            if (number < MyNumber) {
                System.out.println("Your number is too small. Please, try again.");
            } else if (number > MyNumber) {
                System.out.println("Your number is too big. Please, try again.");
            }

        } while (number != MyNumber) ;
            System.out.println("Congratulations " + name + "!" );
    }
}
